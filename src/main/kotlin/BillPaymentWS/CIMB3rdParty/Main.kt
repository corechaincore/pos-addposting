package BillPaymentWS.CIMB3rdParty

import redis.clients.jedis.Jedis
import javax.jws.WebMethod
import javax.jws.WebService
import javax.xml.ws.Endpoint
import javax.jws.WebParam
import javax.jws.WebResult
import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlRootElement
import java.io.FileInputStream
import java.math.RoundingMode
import java.sql.Connection
import java.sql.DriverManager
import java.text.DecimalFormat
import java.util.*
import javax.xml.ws.ResponseWrapper
import kotlin.collections.HashMap
import java.text.SimpleDateFormat
import java.time.LocalDateTime


class WebDriver(){

    fun getParam(): HashMap<String,String>{
        val map: java.util.HashMap<String, String> = java.util.HashMap<String, String>()
        val props = Properties()
        val input = FileInputStream("/Users/fadhilfcr/GoglandProjects/pos-addPosting/service.conf")
        //val input = FileInputStream("/etc/service.conf");
        props.load(input)

        map.put("dbkirim",props.get("db.dbkirim").toString())
        map.put("dbhosting",props.get("db.dbhosting").toString())
        return map

        return map
    }

    fun getDb(): Connection? {

        /*val param = Properties()
        param.put("user", username)
        param.put("password", password)*/

        Class.forName("org.postgresql.Driver").newInstance()
        val connectionUrl = "jdbc:postgresql://localhost:26257/"
        val con = DriverManager.getConnection(connectionUrl,"root","")
        con.autoCommit = false
        //val conn = DriverManager.getConnection("jdbc:microsoft:sqlserver://128.199.238.38:1433;DatabaseName=DBKIRIM","sa","Candisari315!")
        return con
    }

}

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "BillDetailValue", namespace = "https://ws.posindonesia.co.id")
class Layanan {
    constructor() {
        this.responseId  = ""
        this.response  = ""
    }

    @XmlElement(name = "responseId ", namespace = "https://ws.posindonesia.co.id")
    var responseId : String? = null
    @XmlElement(name = "response ", namespace = "https://ws.posindonesia.co.id")
    var response : String? = null
}


@WebService(targetNamespace = "https://ws.posindonesia.co.id")
class MemberEndpoint {

    @ResponseWrapper(localName = "rs_posting",targetNamespace = "https://ws.posindonesia.co.id")
    @WebResult(name = "r_posting", targetNamespace = "https://ws.posindonesia.co.id")
    open fun addPosting(
            @WebParam(name = "userId", targetNamespace = "https://ws.posindonesia.co.id") userId: String,
            @WebParam(name = "password", targetNamespace = "https://ws.posindonesia.co.id") password : String,
            @WebParam(name = "type", targetNamespace = "https://ws.posindonesia.co.id") type : String,
            @WebParam(name = "externalId", targetNamespace = "https://ws.posindonesia.co.id") externalId : String,
            @WebParam(name = "customerId", targetNamespace = "https://ws.posindonesia.co.id") customerId : String,
            @WebParam(name = "serviceId", targetNamespace = "https://ws.posindonesia.co.id") serviceId : String,
            @WebParam(name = "senderName", targetNamespace = "https://ws.posindonesia.co.id") senderName : String,
            @WebParam(name = "senderAddr", targetNamespace = "https://ws.posindonesia.co.id") senderAddr : String,
            @WebParam(name = "senderVill", targetNamespace = "https://ws.posindonesia.co.id") senderVill : String,
            @WebParam(name = "senderSubDist", targetNamespace = "https://ws.posindonesia.co.id") senderSubDist : String,
            @WebParam(name = "senderCity", targetNamespace = "https://ws.posindonesia.co.id") senderCity : String,
            @WebParam(name = "senderProv", targetNamespace = "https://ws.posindonesia.co.id") senderProv : String,
            @WebParam(name = "senderCountry", targetNamespace = "https://ws.posindonesia.co.id") senderCountry : String,
            @WebParam(name = "senderPosCode", targetNamespace = "https://ws.posindonesia.co.id") senderPosCode : String,
            @WebParam(name = "senderEmail", targetNamespace = "https://ws.posindonesia.co.id") senderEmail : String,
            @WebParam(name = "senderPhone", targetNamespace = "https://ws.posindonesia.co.id") senderPhone : String,
            @WebParam(name = "receiverName", targetNamespace = "https://ws.posindonesia.co.id") receiverName : String,
            @WebParam(name = "receiverAddr", targetNamespace = "https://ws.posindonesia.co.id") receiverAddr : String,
            @WebParam(name = "receiverVill", targetNamespace = "https://ws.posindonesia.co.id") receiverVill : String,
            @WebParam(name = "receiverSubDist", targetNamespace = "https://ws.posindonesia.co.id") receiverSubDist : String,
            @WebParam(name = "receiverCity", targetNamespace = "https://ws.posindonesia.co.id") receiverCity : String,
            @WebParam(name = "receiverProv", targetNamespace = "https://ws.posindonesia.co.id") receiverProv : String,
            @WebParam(name = "receiverCountry", targetNamespace = "https://ws.posindonesia.co.id") receiverCountry : String,
            @WebParam(name = "receiverPosCode", targetNamespace = "https://ws.posindonesia.co.id") receiverPosCode : String,
            @WebParam(name = "receiverEmail", targetNamespace = "https://ws.posindonesia.co.id") receiverEmail : String,
            @WebParam(name = "receiverPhone", targetNamespace = "https://ws.posindonesia.co.id") receiverPhone : String,
            @WebParam(name = "orderDate", targetNamespace = "https://ws.posindonesia.co.id") orderDate : String,
            @WebParam(name = "weight", targetNamespace = "https://ws.posindonesia.co.id") weight : String,
            @WebParam(name = "fee", targetNamespace = "https://ws.posindonesia.co.id") fee : String,
            @WebParam(name = "feeTax", targetNamespace = "https://ws.posindonesia.co.id") feeTax : String,
            @WebParam(name = "insurance", targetNamespace = "https://ws.posindonesia.co.id") insurance : String,
            @WebParam(name = "insuranceTax", targetNamespace = "https://ws.posindonesia.co.id") insuranceTax : String,
            @WebParam(name = "itemValue", targetNamespace = "https://ws.posindonesia.co.id") itemValue : String,
            @WebParam(name = "contentDesc", targetNamespace = "https://ws.posindonesia.co.id") contentDesc : String): MutableList<Layanan>? {
        var billDetailList: MutableList<Layanan>? =mutableListOf<Layanan>()
        var model = Layanan()

        if(externalId.equals("")){
            billDetailList!!.add(error("103","BLANK EXTERNAL ID"))
        }else if(customerId.equals("")){
            billDetailList!!.add(error("104","BLANK CUSTOMER ID"))
        }else if(serviceId.equals("")){
            billDetailList!!.add(error("105","BLANK SERVICE ID"))
        }else{
            var db = WebDriver().getDb() as Connection
            var map: java.util.HashMap<String, String> = WebDriver().getParam()
            var dbhosting = map.get("dbhosting").toString()
            var dbkirim = map.get("dbkirim").toString()

            try {
                val sdf = SimpleDateFormat("yyyyMMdd")
                val dates = sdf.format(Date())
                val current = LocalDateTime.now()
                var kdpickup = serviceId+""+dates
                val query:String = "INSERT INTO "+dbhosting+".ITEM_PICKUP (KDPICKUP,IDPENERIMA,KDPELANGGAN,KDLAYANAN,NAMA,KOTA,\n" +
                        " ALAMAT_UTAMA,TELP,KODEPOS,NAMAPERUSAHAAN,KDKANTOR,NIPPOS,BERAT,BEADASAR,HTNB,PPN,PPNHTNB,\n" +
                        " WAKTU,KETERANGAN,NILAIBARANG,NMPENGIRIM,ALAMATPENGIRIM,KELPENGIRIM,KECPENGIRIM,EMAILPENGIRIM,\n" +
                        " KOTAPENGIRIM,PROVPENGIRIM,NEGARAPENGIRIM,HPPENGIRIM,KODEPOSPENGIRIM) VALUES ('$kdpickup','$externalId','$customerId','$serviceId','$receiverName'" +
                        ",'$receiverCity','$receiverAddr','$receiverPhone','$receiverPosCode','$receiverName','-','-','$weight'\n" +
                        ",'$fee','$feeTax','$insurance','$insuranceTax','$current','$contentDesc','$itemValue','$senderName','$senderAddr','$senderVill','$senderSubDist','$senderEmail\n" +
                        "','$senderCity','$senderProv','$senderCountry','$senderPhone','$senderPosCode')"
                with(db) {
                    createStatement().execute(query)
                    commit()
                }

                val queryKirim:String = "INSERT INTO "+dbkirim+".TPESANAN (IDPESANAN,EXT_ID,BARCODE,KDLAYANAN,IDPELANGGAN,NMPENGIRIM,ALAMATPENGIRIM," +
                        "KELPENGIRIM,KECPENGIRIM,EMAILPENGIRIM,KOTAPENGIRIM,PROVPENGIRIM,NEGARAPENGIRIM,HPPENGIRIM,KODEPOSPENGIRIM,NMPENERIMA," +
                        "ALAMATPENERIMA,KELPENERIMA,KECPENERIMA,EMAILPENERIMA,KOTAPENERIMA,PROVPENERIMA,NEGARAPENERIMA,HPPENERIMA,KODEPOSPENERIMA," +
                        "TGLPESAN,KDKANTOR,BERAT,NILAIBARANG,BEADASAR,HTNB,PPN,PPNHTNB,LAINNYA,KDSTATUS,KETERANGAN,ISIBARANG,WAKTU) " +
                        "VALUES ('$kdpickup','$externalId','-','$serviceId','$customerId','$senderName'" +
                        ",'$senderAddr','$senderVill','$senderSubDist','$senderEmail','$senderCity','$senderProv','$senderCountry','$senderPhone'\n" +
                        ",'$senderPosCode','$receiverName','$receiverAddr','$receiverVill','$receiverSubDist','$receiverEmail','$receiverCity','$receiverProv','$receiverCountry'," +
                        "'$receiverPhone','$receiverPosCode','$orderDate\n" +
                        "','','$weight','$itemValue','$fee','$insurance','$feeTax','$insuranceTax','','01','','$contentDesc',TIMESTAMP)"
                with(db) {
                    createStatement().execute(queryKirim)
                    commit()
                }

                val queryBayar:String = "INSERT INTO "+dbkirim+".TPEMBAYARAN (IDPEMBAYARAN,EXT_ID,KDLAYANAN,KDBACKSHEET,IDPELANGGAN," +
                        "KDKANTOR,NIPPOS,BSU,PELUNASAN,BERAT,KDSTATUS,IDTRANSBAYAR,KETERANGAN,WKTLOKAL) " +
                        "VALUES ('$kdpickup','$externalId','204001','0','$customerId','0','0'" +
                        ",'$itemValue','1','0','01','0',NULL,TIMESTAMP)"
                with(db) {
                    createStatement().execute(queryBayar)
                    commit()
                }

            }catch (e:Exception){
                e.printStackTrace()
            }
        }
        return billDetailList
    }
}

fun error(code:String,message:String): Layanan {
    var model = Layanan()
    model.responseId=code
    model.response =message
    return model
}

fun main(args: Array<String>) {
    Endpoint.publish("http://127.0.0.1:7064/", MemberEndpoint())
}